﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UrentMe.Models.ViewModels
{
    public class Inmueble
    {
        public int id_inmueble { get; set; }
        public string descripcion { get; set; }
        public decimal metraje { get; set; }
        public int habitaciones { get; set; }
        public int banos { get; set; }
        public string mascotas { get; set; }
        public string parking { get; set; }
        public decimal precio { get; set; }
        public string direccion { get; set; }
        public string distrito { get; set; }
        public string usuario { get; set; }

        public List<Foto_Inmueble> fotos { get; set; }

    }
}