﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using UrentMe.Models;

using UrentMe.Models.ViewModels;
using UrentMe.Util;

using System.IO;
using System.Drawing;
namespace UrentMe.Controllers
{
    public class HomeController : Controller
    {

        AlquilerCuartosEntities2 bd = new AlquilerCuartosEntities2();
        Utils u = new Utils();
        public ActionResult Index(String id)
        {  

            if (id != null)
            {
                Session["id_usuario"] = null;
                Session["nom_usuario"] = null;
                Session["tipo_usuario"] = null;
                Session["foto_usuario"] = null;
            }
            

            return View(listarInmuebles());
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public List<Foto_Inmueble> ListarFotografias(int idInmueble)
        {

            List<tb_foto_inmueble> lista = bd.tb_foto_inmueble.Where(x => x.id_inmueble == idInmueble).ToList();
            List<Foto_Inmueble> response = new List<Foto_Inmueble>();

            foreach (tb_foto_inmueble f in lista)
            {
                Foto_Inmueble obj = new Foto_Inmueble();
                obj.id_foto = f.id_foto;
                obj.imagen = getFoto(f);

                response.Add(obj);
            }
            return response;

        }

        public String getFoto(tb_foto_inmueble obj)
        {
            string salida = "";
            salida = "data:image/png;base64," + Convert.ToBase64String(obj.imagen);

            return salida;

        }

        public List<Inmueble> listarInmuebles()
        {
            List<tb_inmueble> lista = bd.tb_inmueble.ToList();
            List<Inmueble> response = new List<Inmueble>();

            foreach(tb_inmueble i in lista)
            {
                Inmueble obj = new Inmueble();
                obj.id_inmueble = i.id_inmueble;
                obj.banos = i.banos.Value;
                obj.descripcion = i.descripcion;
                obj.direccion = i.direccion;
                obj.distrito = bd.tb_distrito.Find(i.id_distrito.Value).nombre;
                obj.habitaciones = i.habitaciones.Value;
                obj.mascotas = i.mascotas;
                obj.metraje = i.metraje.Value;
                obj.precio = i.precio.Value;
                tb_usuario usu = bd.tb_usuario.Find(i.id_usuario.Value);

                obj.usuario = usu.nom_usuario;
                obj.fotos = ListarFotografias(i.id_inmueble);

                response.Add(obj);

            }

            return response;
            
        }
    }



}