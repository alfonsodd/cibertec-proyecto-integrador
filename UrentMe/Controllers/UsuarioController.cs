﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Drawing;
using UrentMe.Models;
using UrentMe.Util;
namespace UrentMe.Controllers
{
    public class UsuarioController : Controller
    {
        AlquilerCuartosEntities2 bd = new AlquilerCuartosEntities2();
        Utils u = new Utils();

        public ActionResult LoginRegistro()
        {

            //DESTRUYO LAS VARIABLES PARA QUE NO GUARDE LOS DATOS DE SESION LUEGO DE CERRAR SESION
            Session["id_usuario"] = null;
            Session["nom_usuario"] = null;
            Session["tipo_usuario"] = null;
            Session["foto_usuario"] = null;

            return View(new tb_usuario());
        }
        [HttpPost]
        public ActionResult LoginRegistro(tb_usuario usu)
        {

            if (ModelState.IsValid)
            {
                //ESTE OBJETO SERA EN EL FUTURO PARA EL ADMIN
                //var obj1 = "";

                //ESTA LINEA VALIDA SI EL USUARIO DE TIPO ARRENDADOR EXISTE EN LA BD
                var obj2 = bd.tb_usuario.Where(u =>u.email_usuario.Equals(usu.email_usuario) && u.pass_usuario == usu.pass_usuario && u.id_tipo_usuario == 2).FirstOrDefault();
                //ESTA LINEA VALIDA SI EL USUARIO DE TIPO ARRENDATARIO EXISTE EN LA BD
                var obj3 = bd.tb_usuario.Where(u => u.email_usuario.Equals(usu.email_usuario) && u.pass_usuario == usu.pass_usuario && u.id_tipo_usuario == 3).FirstOrDefault();

                if (obj2 != null)
                {
                    //VARIABLES DE SESION PARA EL MENU
                    Session["id_usuario"] = obj2.id_usuario.ToString();
                    Session["nom_usuario"] = obj2.nom_usuario.ToString();
                    Session["tipo_usuario"] = obj2.id_tipo_usuario.ToString();


                    Session["foto_usuario"] = getFoto();
                    

                    return RedirectToAction("../Home/Index");
                }
                else if (obj3 != null)
                {
                    //VARIABLES DE SESION PARA EL MENU
                    Session["id_usuario"] = obj3.id_usuario.ToString();
                    Session["nom_usuario"] = obj3.nom_usuario.ToString();
                    Session["tipo_usuario"] = obj3.id_tipo_usuario.ToString();


                    Session["foto_usuario"] = getFoto();
                    return RedirectToAction("../Home/Index");
                }
                else
                {
                    //SI DEVUELVE NULL, CREA UN NUEVO USUARIO DE TIPO ARRENDADOR
                    usu.id_tipo_usuario = 2;
                    bd.tb_usuario.Add(usu);
                    bd.SaveChanges();


                    var objRep = bd.tb_usuario.Where(u => u.nom_usuario.Equals(usu.nom_usuario) && u.pass_usuario.Equals(usu.pass_usuario) && u.id_tipo_usuario == 2).FirstOrDefault();
                    if (objRep != null)
                    {
                        //VARIABLES DE SESION PARA EL MENU
                        Session["id_usuario"] = objRep.id_usuario.ToString();
                        Session["nom_usuario"] = objRep.nom_usuario.ToString();
                        Session["tipo_usuario"] = objRep.id_tipo_usuario.ToString();
                        Session["foto_usuario"] = objRep.foto_usuario.ToString();
                        return RedirectToAction("../Home/Index");
                    }

                }


            }


            return View();

        }


        public ActionResult Perfil(int id)
        {
            if (Session["id_usuario"] != null)
            {
                tb_usuario u = bd.tb_usuario.Find(id);
                return View(u);
            }
            else
            {
                return RedirectToAction("../Home/Index");
            }
        }
        [HttpPost]
        public ActionResult Perfil(HttpPostedFileBase inputFotoPerfil, [Bind(Include = "nom_usuario,ape_usuario,email_usuario,cel_usuario")] tb_usuario usu)
        {
            if (Session["id_usuario"] != null)
            {
                if(inputFotoPerfil != null)
                {
                    EditFoto(inputFotoPerfil);
                }
                
                tb_usuario uTemporal = bd.tb_usuario.Find(Convert.ToInt32(Session["id_usuario"]));

                uTemporal.nom_usuario = usu.nom_usuario;
                uTemporal.ape_usuario = usu.ape_usuario;
                uTemporal.email_usuario = usu.email_usuario;
                uTemporal.cel_usuario = usu.cel_usuario;



                Session["nom_usuario"] = usu.nom_usuario;
                Session["foto_usuario"] = getFoto();
                
                bd.SaveChanges();
                return RedirectToAction("../Home/Index");
            }
            else
            {
                return RedirectToAction("../Home/Index");
            }
        }

        public void EditFoto(HttpPostedFileBase inputFotoPerfil)
        {

            int datos = Int32.Parse(Session["id_usuario"] as String);

            Image i = Image.FromStream(inputFotoPerfil.InputStream, true, true);

            byte[] data = u.imageToByteArray(i);
    
            tb_usuario objUsuario = bd.tb_usuario.First(x => x.id_usuario == datos);
            objUsuario.imagen = data;
            bd.SaveChanges();
        }

        public String getFoto()
        {
            int datos = Int32.Parse(Session["id_usuario"] as String);
            string salida = "";
            tb_usuario u = bd.tb_usuario.First(x => x.id_usuario == datos);
            if (u.imagen != null)
            {
                salida = "data:image/png;base64," +  Convert.ToBase64String(u.imagen);
            }
            else { salida = u.foto_usuario; }

            return salida;

        }







    }
}
