﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UrentMe.Models;

using UrentMe.Models.ViewModels;
using UrentMe.Util;

using System.IO;
using System.Drawing;
namespace UrentMe.Controllers
{
    public class InmuebleController : Controller
    {


        AlquilerCuartosEntities2 bd = new AlquilerCuartosEntities2();
        Utils u = new Utils();

        public ActionResult Publicar()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Publicar(tb_inmueble inmueble)
        {
            inmueble.id_usuario = Int32.Parse( Session["id_usuario"] as String);
            bd.tb_inmueble.Add(inmueble);


            bd.SaveChanges();

            int value = int.Parse(bd.tb_inmueble
                            .OrderByDescending(p => p.id_inmueble)
                            .Select(r => r.id_inmueble)
                            .First().ToString());

            Session["id_inmueble"] = value;

            return RedirectToAction("Fotos");
        }

        public ActionResult Fotos()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Fotos(HttpPostedFileBase fotografia)
        {
            AgregarFotoInmueble(fotografia);

            return RedirectToAction("Fotos");
        }

        public JsonResult ListarFotografias()
        {
            int id_inmueble = int.Parse(Session["id_inmueble"].ToString());

            List<tb_foto_inmueble> lista = bd.tb_foto_inmueble.Where(x => x.id_inmueble == id_inmueble).ToList();
            List<Foto_Inmueble> response = new List<Foto_Inmueble>();

            foreach (tb_foto_inmueble f in lista)
            {
                Foto_Inmueble obj = new Foto_Inmueble();
                obj.id_foto = f.id_foto;
                obj.imagen = getFoto(f);

                response.Add(obj);
            }
            return Json(response, JsonRequestBehavior.AllowGet);

        }

        [HttpPost]
        public JsonResult EliminarFotografia(int codigo)
        {
            tb_foto_inmueble obj = bd.tb_foto_inmueble.Find(codigo);
            bd.tb_foto_inmueble.Remove(obj);
            bd.SaveChanges();
            return Json("ok");
            
        }

        public JsonResult ListarDistritos()
        {
            List<tb_distrito> lista = bd.tb_distrito.ToList();
            return Json(lista, JsonRequestBehavior.AllowGet);
        }


        public void AgregarFotoInmueble(HttpPostedFileBase foto)
        {

            int id_inmueble = int.Parse(Session["id_inmueble"].ToString());

            Image i = Image.FromStream(foto.InputStream, true, true);

            byte[] data = u.imageToByteArray(i);

            tb_foto_inmueble obj = new tb_foto_inmueble();
            obj.imagen = data;
            obj.id_inmueble = id_inmueble;

            bd.tb_foto_inmueble.Add(obj);
            bd.SaveChanges();
        }

        public String getFoto(tb_foto_inmueble obj)
        {
            string salida = "";
            salida = "data:image/png;base64," + Convert.ToBase64String(obj.imagen);
              
            return salida;

        }

    }
}