﻿window.onload =  function () {


    var app_id = '452112072061690';
    var scopes = 'email';

    var btn_login = '<a href="#" id="login" class="btn btn-primary">Iniciar sesión</a>';

    var div_session = "<div id='facebook-session'>" +
        "<strong></strong>" +
        "<img>" +
        "<a href='#' id='logout' class='btn btn-danger'>Cerrar sesión</a>" +
        "</div>";





   
        window.fbAsyncInit = function() {
            FB.init({
                appId: app_id,
                autoLogAppEvents: true,
                xfbml: true,
                version: 'v4.0'
            });
        };
      
  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return ;}
        js = d.createElement(s); js.id = id;
        js.src = "https://connect.facebook.net/en_US/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
      }(document, 'script', 'facebook-jssdk'));




    var statusChangeCallback = function(response, callback) {
        console.log(response);

        if (response.status === 'connected') {
            getFacebookData();
        } else {
            callback(false);
        }
    }

    var checkLoginState = function (callback) {
        FB.getLoginStatus(function (response) {
            statusChangeCallback(response, function (data) {
                callback(data);
            });
        });
    }


    var person = { userID: "", name: "", accessToken: "", picture: "", email: "" };

    var getFacebookData = function () {
        FB.api('/me?fields=id,name,email,picture.type(large)', function (response) {
            //$('#login').after(div_session);
            //$('#login').remove();
            //$('#User_name').text(response.name);
            
            //$('#User_name').text("dddd");
            //$('#User_foto').attr('src', 'http://graph.facebook.com/' + response.id + '/picture?type=large');
            person.userID = response.id;
            person.name = response.name;
            person.email = response.email;
            person.picture = response.picture.data.url;
            console.log(person.picture);
            
            $('#User_name').val(person.name);
            $('#User_email').val(person.email);
            $('#User_pass').val(person.userID);
            $('#User_foto').attr('src', person.picture);
            $('#UserFotoRegistro').val(person.picture);

        });
    }


   
    var facebookLogin = function () {
        checkLoginState(function (response) {
            if (!response) {
                FB.login(function (response) {
                    if (response.status === 'connected')
                        person.userID = response.authResponse.userID;
                        person.accessToken = response.authResponse.accessToken;
                        getFacebookData();
                }, { scope: 'public_profile, email' });
            }
        })
    }



    var facebookLogout = function () {
        FB.getLoginStatus(function (response) {
            if (response.status === 'connected') {
                FB.logout(function (response) {
                    $('#facebook-session').before(btn_login);
                    $('#facebook-session').remove();
                })
            }
        }
    )}



    $(document).on('click', '#login', function (e) {
        e.preventDefault();

        facebookLogin();
    })


    $(document).on('click', '#logout', function (e) {
        e.preventDefault();

        if (confirm("¿Está seguro?"))
            facebookLogout();
        else
            return false;
    })








}
