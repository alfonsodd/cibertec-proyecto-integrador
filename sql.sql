
use master
go

create database AlquilerCuartos
go

use AlquilerCuartos
go

create table tb_tipoUsuario
(
id_tipo_usuario int primary key identity(1,1),
des_tipo_usuario varchar(50)
)
go


create table tb_usuario
(
id_usuario int primary key identity(1,1),
nom_usuario varchar(50),
ape_usuario varchar(50),
email_usuario varchar(50),
pass_usuario varchar(50),
cel_usuario varchar(20),
foto_usuario varchar(max),
id_tipo_usuario int foreign key references tb_tipoUsuario(id_tipo_usuario),
imagen varbinary(max) default null
)
go

create table tb_distrito(
id_distrito int primary key identity(1,1),
nombre varchar(100)
)
go

create table tb_inmueble
(
id_inmueble int primary key identity(1,1),
descripcion varchar(max),
metraje decimal(6,2),
habitaciones int,
banos int,
mascotas char(2),
parking char(2),
precio decimal(6,2),
direccion varchar(250),
id_distrito int foreign key references tb_distrito(id_distrito),
id_usuario int foreign key references tb_usuario(id_usuario)
)
go

create table tb_foto_inmueble(
id_foto int primary key identity(1,1),
imagen varbinary(max) default null,
id_inmueble int foreign key references tb_inmueble(id_inmueble)
)
go

insert into tb_tipoUsuario values('Admin')
insert into tb_tipoUsuario values('Arrendador')
insert into tb_tipoUsuario values('Arrendatario')
insert into tb_usuario values('Usuario1','Apellido','prueba1@gmail.com','Usuario1','900874566','https://img.icons8.com/carbon-copy/2x/user.png',1,null)
insert into tb_usuario values('Usuario2',null,'prueba2@gmail.com','Usuario2','975866478','https://img.icons8.com/carbon-copy/2x/user.png',2,null)

/*Distritos*/
insert into tb_distrito values('Independencia')

insert into tb_distrito values('Los Olivos')

insert into tb_distrito values('Miraflores')

insert into tb_distrito values('SJL')





select * from tb_tipoUsuario
select * From tb_usuario
select * From tb_inmueble
select * From tb_foto_inmueble

delete from tb_inmueble where id_inmueble >0
delete from tb_foto_inmueble where id_foto > 0

----truncate table tb_usuario